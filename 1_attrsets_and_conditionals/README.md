# Lesson 2: Advanced Nix Concepts - Attribute Sets and Conditionals

## Overview
In this lesson, we'll explore more advanced concepts in the Nix language,
including attribute sets and conditionals. These concepts are crucial for
building more complex and flexible Nix expressions.

## Lesson Content

### 2.1 Attribute Sets
Attribute sets are collections of key-value pairs. They are used to represent
sets of attributes or properties. Open the Nix REPL and try the following:

```nix
# Define an attribute set
let
  person = { name = "John"; age = 30; };
in
  person.name
```

### 2.2 Conditionals
Conditionals allow you to make decisions in your Nix expressions. Use the
following examples to understand their usage:

```nix
# Basic conditional
let
  mod = import ../0_introduction/mod.nix;
  isEven = n: if (mod n 2) == 0 then "even" else "odd";
in
  isEven 4

# Conditional with multiple branches
let
  grade = score: if score >= 90 then "A"
                  else if score >= 80 then "B"
                  else "C";
in
  grade 85
```

### 2.3 Exercises
1. Create an attribute set `animal` with keys `type` and `sound`.
2. Write a function `makeSound` that takes an `animal` attribute set as an
argument and returns the sound.
3. Use a conditional to determine if a given number is positive, negative, or
zero.

### 2.4 Solutions
1. **animal.nix**:
   ```nix
   let
     animal = { type = "cat"; sound = "meow"; };
   in
     animal
   ```

2. **makeSound.nix**:
   ```nix
   animal: animal.sound
   ```

3. **checkNumber.nix**:
   ```nix
   num: if num > 0 then "positive"
        else if num < 0 then "negative"
        else "zero"
   ```

## Conclusion
This lesson introduced attribute sets and conditionals, expanding your
understanding of Nix expressions. In the next lesson, we'll dive into the power
of functions and how they can be used for more dynamic configurations.
