# Lesson 3: Functions and Higher-Order Functions in Nix

## Overview
In this lesson, we'll delve deeper into the power of functions in the Nix language. We'll cover the basics of functions, higher-order functions, and their application in creating reusable and composable Nix expressions.

## Lesson Content

### 3.1 Basics of Functions
Functions in Nix are first-class citizens, meaning they can be assigned to variables and passed as arguments to other functions. Start with simple examples:

```nix
# Basic function
let
  square = x: x * x;
in
  square 5
```

### 3.2 Higher-Order Functions
Nix supports higher-order functions, allowing functions to take other functions as arguments or return functions. Explore the concept with the following example:

```nix
# Higher-order function
let
  applyTwice = f: x: f (f x);
  square = x: x * x;
in
  applyTwice square 3
```

### 3.3 Function Composition
Compose functions to create more complex behaviors:

```nix
# Function composition
let
  addOne = x: x + 1;
  multiplyByTwo = x: x * 2;
  addOneAndDouble x =  multiplyByTwo (addOne x);
in
  addOneAndDouble 3
```

### 3.4 Exercises
1. Create a function `multiplyBy` that takes a number `n` and returns a function that multiplies its argument by `n`.
2. Use the `multiplyBy` function to create a function `double` that doubles its argument.
3. Write a higher-order function `compose` that takes two functions `f` and `g` and returns their composition.

### 3.5 Solutions
1. **multiplyBy.nix**:

2. **double.nix**:

3. **compose.nix**:

## Conclusion
This lesson expanded your knowledge of functions in Nix, including higher-order functions and function composition. In the next lesson, we'll focus on working with files, imports, and building more complex Nix expressions.
