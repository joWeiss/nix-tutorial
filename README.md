# Nix Language Learning Series Overview

Author: Jonas Weißensel
Last Update: 2023-11-16

## Lesson 1: Introduction to Nix and Basic Expressions

- **Content:**
  - Nix Data Types
  - Variables
  - Functions
- **Exercises:**
  1. Create variables
  2. Write a function
  3. Use functions and variables together
- **Solutions:**
  - mod.nix
- **Conclusion:**
  Basics of Nix expressions.

## Lesson 2: Advanced Nix Concepts - Attribute Sets and Conditionals

- **Content:**
  - Attribute Sets
  - Conditionals
- **Exercises:**
  1. Create an attribute set
  2. Write a function for attribute sets
  3. Use conditionals
- **Solutions:**
  - animal.nix, makeSound.nix, checkNumber.nix
- **Conclusion:**
  Attribute sets and conditionals in Nix.

## Lesson 3: Functions and Higher-Order Functions in Nix

- **Content:**
  - Basic of functions
  - Higher-order functions
  - Function composition
- **Excercises:**
  1. Create a function
  2. Use the newly created function
  3. Write a higher-order function
- **Solutions:**
  - multiplyBy.nix, double.nix, compose.nix
- **Conclusions:**:
  Functions and Higher-Order Functions in Nix.

## Lesson 4: flake.nix

WIP

## Lesson 5: The nix CLI

TODO
(As long as this is missing, go and read [this](https://serokell.io/blog/practical-nix-flakes))

# Resources

- [Nix Language Manual (Official)](https://nixos.org/manual/nix/stable/language)
- [Nixpkgs Manual (Official)](https://nixos.org/manual/nixpkgs/stable)
- [Flakes](https://nixos.wiki/wiki/Flakes)
- [Trivial Builders (in nixpkgs)](https://ryantm.github.io/nixpkgs/builders/trivial-builders/)
- [Overview of the Nix Language](https://nixos.wiki/wiki/Overview_of_the_Nix_Language)
- [Nix Language: Tips & Tricks](https://nixos.wiki/wiki/Nix_Language:_Tips_%26_Tricks)
- [How to learn nix (Blog series)](https://ianthehenry.com/posts/how-to-learn-nix/)


# Development

Install `direnv` and `direnv allow` this folder for autoloading of the flake.
