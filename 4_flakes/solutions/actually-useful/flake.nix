{
  description = "A new flake";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

  outputs = {
    self,
    nixpkgs,
  }: let
    linux = "x86_64-linux";
    macOS = "aarch64-darwin";
    linuxPkgs = nixpkgs.legacyPackages.${linux};
    macOSPkgs = nixpkgs.legacyPackages.${macOS};
  in {
    devShells.${linux}.default = linuxPkgs.mkShell {
      name = "${linux}-nix-shell";
      packages = with linuxPkgs; [
        (python3.withPackages (ps: [ps.black ps.nox ps.isort ps.flake8 ps.autoflake ps.mypy]))
      ];
    };
    devShells.${macOS}.default = macOSPkgs.mkShell {
      name = "${macOS}-nix-shell";
      packages = with macOSPkgs; [
        (python3.withPackages (ps: [ps.black ps.nox ps.isort ps.flake8 ps.autoflake ps.mypy]))
      ];
    };
  };
}
