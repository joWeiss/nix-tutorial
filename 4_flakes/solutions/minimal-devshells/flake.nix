{
  description = "A new flake";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

  outputs = { self, nixpkgs }: let
    linux = "x86_64-linux";
    macOS = "aarch64-darwin";
  in {
    devShells.${linux}.default = nixpkgs.legacyPackages.${linux}.mkShell {name ="${linux}-nix-shell";};
    devShells.${macOS}.default = nixpkgs.legacyPackages.${macOS}.mkShell {name ="${macOS}-nix-shell";};
  };
}