# Lesson 4: Flakes

## Overview

In this lesson, we'll introduce `flake.nix`, a powerful way to manage Nix
projects and configurations. We'll explore its structure and cover advanced
concepts such as overlays and dependencies.

### 4.1 Understanding `flake.nix`

A `flake.nix` file is a declarative specification for a Nix flake. Start by
creating a simple flake:

```nix
{
  description = "A new flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, }: {
    devShells.x86_64-linux.default = nixpkgs.legacyPackages.x86_64-linux.mkShell {
        packages = [nixpkgs.legacyPackages.x86_64-linux.hello];
      };
    });
  };
}
```

#### Schema

It has 4 top-level attributes:

- `description` is a string describing the flake.
- `inputs` is an attribute set of all the dependencies of the flake. The schema
is described in [flake-inputs](https://nixos.org/manual/nix/stable/command-ref/new-cll/nix3-flake.html#flake-inputs) 
and [flake-references](https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-flake.html#flake-references).
- `outputs` is a function of one argument that takes an attribute set of all
the realized inputs, and outputs another attribute set whose schema is described
below.
- `nixConfig` is an attribute set of values which reflect the values given to
nix.conf. This can extend the normal behavior of a user's nix experience by
adding flake-specific configuration, such as a binary cache.

##### Output schema

`outputs` defines a function, which receives the resolved inputs and returns the
outputs according to the following schema (copied from [here](https://nixos.wiki/wiki/Flakes)):

> (Note: `<system>` is shorthand for a system string like "x86_64-linux" or "aarch64-darwin")  
> (Note: `<name>` is an attribute name like "hello")  
> (Note: `<flake>` is a flake name)  
> (Note: `<store-path>` is a `/nix/store/...` path)  

```nix
{ self, ... }@inputs:
{
  # Executed by `nix flake check`
  checks."<system>"."<name>" = derivation;
  # Executed by `nix build .#<name>`
  packages."<system>"."<name>" = derivation;
  # Executed by `nix build .`
  packages."<system>".default = derivation;
  # Executed by `nix run .#<name>`
  apps."<system>"."<name>" = {
    type = "app";
    program = "<store-path>";
  };
  # Executed by `nix run . -- <args?>`
  apps."<system>".default = { type = "app"; program = "..."; };

  # Formatter (alejandra, nixfmt or nixpkgs-fmt)
  formatter."<system>" = derivation;
  # Used for nixpkgs packages, also accessible via `nix build .#<name>`
  legacyPackages."<system>"."<name>" = derivation;
  # Overlay, consumed by other flakes
  overlays."<name>" = final: prev: { };
  # Default overlay
  overlays.default = final: prev: { };
  # Nixos module, consumed by other flakes
  nixosModules."<name>" = { config }: { options = {}; config = {}; };
  # Default module
  nixosModules.default = { config }: { options = {}; config = {}; };
  # Used with `nixos-rebuild --flake .#<hostname>`
  # nixosConfigurations."<hostname>".config.system.build.toplevel must be a derivation
  nixosConfigurations."<hostname>" = {};
  # Used by `nix develop .#<name>`
  devShells."<system>"."<name>" = derivation;
  # Used by `nix develop`
  devShells."<system>".default = derivation;
  # Hydra build jobs
  hydraJobs."<attr>"."<system>" = derivation;
  # Used by `nix flake init -t <flake>#<name>`
  templates."<name>" = {
    path = "<store-path>";
    description = "template description goes here?";
  };
  # Used by `nix flake init -t <flake>`
  templates.default = { path = "<store-path>"; description = ""; };
}
```

### 4.2 Exercices 

#### 4.2.1 Writing a small `flake.nix` for a multi-platform devShell

- The flake should define a `linux` and a `macOS` variable, so
that the actual OS names don't have to be repeated all over the flake.nix.
- The value of `linux` should be `x86_64-linux`.
- The value of `macOS` should be `aarch64-darwin`.
- To define a `devShell`, we have to write an output for the attribute
`devShells.<system>.<name>`.
- To define a `devShell`, we have to utilize the `mkShell` function
from `nixpkgs`, so we have to somehow reference that.

#### 4.2.2 Stop repeating yourself so much.

As you may have noticed, a lot of code looks exactly the same.
Since `nix` is lazily evaluated, we can introduce variables
for the `nixpkgs` attribute set for all supported systems;

```nix
let
  system = "x86_64-linux";
  pkgs = nixpkgs.legacyPackges.${system};
in
 pkgs.mkShell {
  packages = [pkgs.htop];
}
```

#### 4.2.3 Our first useful devShell

Write a `flake.nix` that uses your freshly acquired knowledge to define a
`devShell` which provides the `python3` package including the following python
modules: "black", "nox", "isort", "autoflake", "flake8"

