let
  mod = x: y: let
    d = builtins.div x y;
    r = x - (y * d);
  in
    r;
in
  mod
