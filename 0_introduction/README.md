# Lesson 1: Introduction to Nix and Basic Expressions

## Overview
In this lesson, you will get started with the Nix language by understanding
basic expressions and using the Nix REPL. We'll cover simple data types,
functions, and variables.

## Prerequisites
Make sure you have Nix installed on your system. You can install it by following
the instructions on the official [Nix website](https://nixos.org/download.html).

## Lesson Content

### 1.1 Nix Data Types
Nix has several built-in data types. Open a terminal and start the Nix REPL:

```bash
nix repl
```

In the REPL, try the following expressions:

```nix
# Integers
1 + 2

# Strings
"Hello, " + "Nix!"

# Multi-line Strings
''
  Hello,
  this is a multi-line string.
    -- Jonas
''

# Lists
[ 1 2 3 ]

# Attributes
{ key = "value"; }

# Paths & URIs
./README.md
/usr/bin/env
https://github.com/nixos/nixpkgs
github:nixos/nixpkgs/nixos-23.11

# Boolean
true
false

# Null
null
```

### 1.2 Variables
Define variables and use them in expressions:

```nix
# Define a variable
let
  greeting = "Hello, ";
in
  greeting + "Nix!"
```

### 1.3 Functions
Create and use functions:

```nix
# Define a function
let
  greet = name: "Hello, " + name;
in
  greet "Nix"
```

### 1.4 Exercises
1. Create a variable `num` with the value `42` and another variable `message`
with the value `"The answer is "`.
2. Write a function `combine` that takes two arguments (`a` and `b`) and
concatenates them into a string.
3. Use the `combine` function to create a message combining `message` and `num`.
4. Write a function `mod` in a new file `math.nix` that calculates the remainder
of an integer division of two integers.

### 1.5 Solutions
1. `num.nix`:
   ```nix
   num = 42;
   ```

   `message.nix`:
   ```nix
   message = "The answer is ";
   ```

2. `combine.nix`:
   ```nix
   combine = a: b: a + b
   ```

3. `combinedMessage.nix`:
   ```nix
   let
     num = 42;
     message = "The answer is: ";
     combine = a: b: a + b;
   in
     combine message (toString num)
   ```

4. `mod.nix`:
   ```nix
    let
     mod = x: y: let
       divider = builtins.div x y;
       remaining = x - (y * divider);
     in
       remaining;
    in mod
   ```

## Conclusion

This lesson covered the basics of Nix expressions, including data types,
variables, and functions. In the next lesson, we will explore more advanced
topics such as attribute sets and conditionals.
