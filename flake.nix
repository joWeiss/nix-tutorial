{
  description = "Tutorials to learn nix";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
  outputs = {
    self,
    nixpkgs,
  }: let
    linux = "x86_64-linux";
    macOS = "aarch64-darwin";

    forEachSystem = systems: f:
      nixpkgs.lib.genAttrs systems
      (system:
        f (import nixpkgs {
          inherit system;
          config.allowUnfree = true;
        }));
  in {
    devShells = forEachSystem [linux macOS] (pkgs: {
      default = pkgs.mkShell {
        packages = with pkgs; [
          alejandra # nix formatter
          nil # nix language server
          (vscode-with-extensions.override {
            vscodeExtensions = with vscode-extensions; [
              bbenoist.nix
              jnoortheen.nix-ide
              kamadorueda.alejandra
              arrterian.nix-env-selector
              editorconfig.editorconfig
            ];
          })
        ];
      };
    });
  };
}
